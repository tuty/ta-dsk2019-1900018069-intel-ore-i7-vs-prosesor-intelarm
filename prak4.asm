.model small
.code
org 100h
mulai:
	jmp cetak
	hello 	db 'SELAMAT DATANG '
		db 'DI BAHASA ASSEMBLY'
		db '$'
cetak:
	mov ah, 09h
	mov dx, offset hello
	int 21h
habis:
	int 20h
end mulai